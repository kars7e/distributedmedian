package dmedian

// Node isolates operations that should be executed within a node
type Node interface {
	// MinMax returns minimum and maximum values for the current state within the node.
	MinMax() (int, int)
	// Len returns the length of the currently stored data.
	Len() int
	// Pivot returns a value within the data set that can be used to  pivot around.
	Pivot() int
	// Reset allows reuse of the node for another calculation.
	Reset()
	// FilterGreaterOrEq alters internal state to only keep values greater or equal to the given argument.
	FilterGreaterOrEq(int)
	// FilterLessThan alters internal state to only keep values less than the given argument.
	FilterLessThan(int)
	// CountLessThan returns the number of all values lower than the given argument.
	CountLessThan(int) int
	// Debug returns information about internal state, useful for debugging.
	Debug() string
}
