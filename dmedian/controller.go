package dmedian

import (
	"fmt"
	"sync"
	"sync/atomic"
)

// Controller manages distribution of calculations across nodes
type Controller struct {
	nodes        []Node
	initialNodes []Node
	length       int
}

// Median calculates the median across all nodes. If total number of elements on all nodes is even,
// median is calculated as average of n/2th and n/2+1th statistic, whereas if the number of elements is odd,
// n/2+1 statistic is returned. n is the number of all elements on all nodes.
func (c *Controller) Median() float64 {
	c.reset()
	if c.length%2 == 0 {
		left := c.selectNthStat(c.length / 2)
		c.reset()
		right := c.selectNthStat(c.length/2 + 1)

		if left == right {
			return float64(left)
		} else {
			return float64(left+right) / 2
		}
	}
	return float64(c.selectNthStat(c.length/2 + 1))
}

// RegisterNode adds node to the controller
func (c *Controller) RegisterNode(node Node) {
	c.nodes = append(c.nodes, node)
	c.initialNodes = append(c.initialNodes, node)
	c.length += node.Len()
}

// Debug prints debugging information from all nodes
func (c *Controller) Debug() {
	for _, node := range c.nodes {
		fmt.Println(node.Debug())
	}
}

// selectNthStat returns n-th statistic across all nodes
func (c *Controller) selectNthStat(n int) int {
	c.pruneEmptyNodes()
	min, max := c.minMax()
	if min == max || n == 1 {
		// we have only one value left or we are looking for 1st order statistic,
		// which is the minimum
		return min
	}

	return c.partition(n)
}

// partition
func (c *Controller) partition(n int) int {
	pivot := c.pivot()
	count := c.countLessThan(pivot)
	pivotPosition := count + 1
	if count == 0 {
		c.rotate()
		return c.selectNthStat(n)
	}

	if pivotPosition == n {
		return pivot
	}

	// check which side of pivot has more elements. that side has the target statistic.
	// we can ignore the elements on the other side, reducing search space.
	if pivotPosition > n {
		c.filterLessThan(pivot)
		return c.selectNthStat(n)
	} else {
		c.filterGreaterOrEq(pivot)
		c.rotate()
		return c.selectNthStat(n - count)
	}

}

func (c *Controller) minMax() (min, max int) {
	type response struct{ min, max int }
	responses := make(chan response, len(c.nodes))

	for i := range c.nodes {
		go func(node Node) {
			nodeMin, nodeMax := node.MinMax()
			responses <- response{nodeMin, nodeMax}
		}(c.nodes[i])
	}

	initialValue := <-responses
	min, max = initialValue.min, initialValue.max
	for i := 0; i < len(c.nodes)-1; i++ {
		currentValue := <-responses
		if currentValue.min < min {
			min = currentValue.min
		}
		if currentValue.max > max {
			max = currentValue.max
		}
	}
	return
}

// filterGreaterOrEq tells all nodes to filter their current data set to only
// include values greater or equal to target.
func (c *Controller) filterGreaterOrEq(target int) {
	wg := sync.WaitGroup{}
	for _, node := range c.nodes {
		wg.Add(1)
		go func(node Node) {
			node.FilterGreaterOrEq(target)
			wg.Done()
		}(node)
	}
	wg.Wait()
}

// filterLessThan tells all nodes to filter their current data set to only
// include values less than target.
func (c *Controller) filterLessThan(target int) {
	wg := sync.WaitGroup{}
	for _, node := range c.nodes {
		wg.Add(1)
		go func(node Node) {
			node.FilterLessThan(target)
			wg.Done()
		}(node)
	}
	wg.Wait()
}

// countLessThan returns the number of elements across all nodes that are less than target.
func (c *Controller) countLessThan(target int) int {
	wg := sync.WaitGroup{}
	count := int64(0)
	for _, node := range c.nodes {
		wg.Add(1)
		go func(node Node) {
			value := node.CountLessThan(target)
			atomic.AddInt64(&count, int64(value))
			wg.Done()
		}(node)
	}
	wg.Wait()
	return int(count)
}

// reset tells all nodes to reset their state to the initial state
func (c *Controller) reset() {
	c.nodes = c.initialNodes
	wg := sync.WaitGroup{}
	for _, node := range c.nodes {
		wg.Add(1)
		go func(node Node) {
			node.Reset()
			wg.Done()
		}(node)
	}
	wg.Wait()
	return
}

func (c *Controller) pivot() int {
	return c.nodes[0].Pivot()
}

// pruneEmptyNodes removes nodes without any data left from the current state.
func (c *Controller) pruneEmptyNodes() {
	var nodesWithData []Node
	for _, node := range c.nodes {
		if node.Len() > 0 {
			nodesWithData = append(nodesWithData, node)
		}
	}
	c.nodes = nodesWithData
}

// rotate rotates the nodes in the current state. used for better pivot value distribution.
func (c *Controller) rotate() {
	c.nodes = append(c.nodes[1:], c.nodes[0])
}
