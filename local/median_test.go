package local

import (
	"math/rand"
	"testing"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func TestSimpleValues(t *testing.T) {
	testCases := []struct {
		input    []int
		expected float64
	}{
		{input: []int{1, 2, 3}, expected: 2},
		{input: []int{1, 1, 1}, expected: 1},
		{input: []int{1}, expected: 1},
		{input: []int{1, 2, 2, 3}, expected: 2},
		{input: []int{1, 2, 3, 1}, expected: 1.5},
		{input: []int{10, 10, 10, 10, 6, 5, 5, 5, 5}, expected: 6},
	}

	t.Run("SelectionMedian", func(t *testing.T) {
		for _, testCase := range testCases {
			result := Median(testCase.input)
			if result != testCase.expected {
				t.Errorf("expected: %f, got: %f for input: %+v", testCase.expected, result, testCase.input)
			}
		}
	})

	t.Run("SortedMedian", func(t *testing.T) {
		for _, testCase := range testCases {
			result := MedianSorted(testCase.input)
			if result != testCase.expected {
				t.Errorf("expected: %f, got: %f for input: %+v", testCase.expected, result, testCase.input)
			}
		}
	})

}

func TestRandomCrossCheck(t *testing.T) {
	type testCase struct {
		input    []int
		expected float64
	}

	var testCases []testCase
	for i := 0; i < 10; i++ {
		data := generateRandomInts(100, 100)
		testCases = append(testCases, testCase{
			input:    data,
			expected: Median(data),
		})
	}

	t.Run("CrossCheck", func(t *testing.T) {
		for _, testCase := range testCases {
			result := MedianSorted(testCase.input)
			if result != testCase.expected {
				t.Errorf("SelectionMedian returned: %f, SortedMedian returned: %f for input: %+v", testCase.expected, result, testCase.input)
			}
		}
	})

}

func generateRandomInts(num int, max int) []int {
	list := make([]int, 0, num)
	for i := 0; i < num; i++ {
		list = append(list, rand.Intn(max))
	}
	return list
}
