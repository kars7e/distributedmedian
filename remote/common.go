package remote

import (
	"distributedmedian/dmedian"
)

// MinMaxResponse encapsulates response for MinMax Remote Call
type MinMaxResponse struct {
	Min, Max int
}

// Wrapper implements RPC interface from net/rpc library
type Wrapper struct {
	dmedian.Node
}

func (w *Wrapper) MinMax(_ int, response *MinMaxResponse) error {
	min, max := w.Node.MinMax()
	*response = MinMaxResponse{Min: min, Max: max}
	return nil
}

func (w *Wrapper) Len(_ int, response *int) error {
	*response = w.Node.Len()
	return nil
}

func (w *Wrapper) Pivot(_ int, response *int) error {
	*response = w.Node.Pivot()
	return nil
}

func (w *Wrapper) Reset(_ int, _ *int) error {
	w.Node.Reset()
	return nil
}

func (w *Wrapper) FilterGreaterOrEq(target int, _ *int) error {
	w.Node.FilterGreaterOrEq(target)
	return nil
}

func (w *Wrapper) FilterLessThan(target int, _ *int) error {
	w.Node.FilterLessThan(target)
	return nil
}

func (w *Wrapper) CountLessThan(target int, response *int) error {
	*response = w.Node.CountLessThan(target)
	return nil
}

func (w *Wrapper) Debug(_ int, response *string) error {
	*response = w.Node.Debug()
	return nil
}
