package remote

import (
	"log"
	"net/rpc"
)

// Node implements dmedian.Node interface using rpc.client
type Node struct {
	c *rpc.Client
}

func NewNode(hostport string) (*Node, error) {
	client, err := rpc.DialHTTP("tcp", hostport)
	if err != nil {
		return nil, err
	}
	return &Node{c: client}, nil
}

func (node *Node) MinMax() (int, int) {
	var response MinMaxResponse
	err := node.c.Call("Node.MinMax", 0, &response)
	if err != nil {
		log.Fatalf("Error calling remote function MinMax: %s", err)
	}
	return response.Min, response.Max
}

func (node *Node) Len() int {
	var response int
	err := node.c.Call("Node.Len", 0, &response)
	if err != nil {
		log.Fatalf("Error calling remote function Len: %s", err)
	}
	return response
}

func (node *Node) Pivot() int {
	var response int
	err := node.c.Call("Node.Pivot", 0, &response)
	if err != nil {
		log.Fatalf("Error calling remote function Pivot: %s", err)
	}
	return response
}

func (node *Node) Reset() {
	var response int
	err := node.c.Call("Node.Reset", 0, &response)
	if err != nil {
		log.Fatalf("Error calling remote function Reset: %s", err)
	}
}

func (node *Node) FilterGreaterOrEq(target int) {
	var response int
	err := node.c.Call("Node.FilterGreaterOrEq", target, &response)
	if err != nil {
		log.Fatalf("Error calling remote function FilterGreaterOrEq: %s", err)
	}
}

func (node *Node) FilterLessThan(target int) {
	var response int
	err := node.c.Call("Node.FilterLessThan", target, &response)
	if err != nil {
		log.Fatalf("Error calling remote function FilterLessThan: %s", err)
	}
}

func (node *Node) CountLessThan(pivot int) int {
	var response int
	err := node.c.Call("Node.CountLessThan", pivot, &response)
	if err != nil {
		log.Fatalf("Error calling remote function CountLessThan: %s", err)
	}
	return response
}

func (node *Node) Debug() string {
	var response string
	err := node.c.Call("Node.Debug", 0, &response)
	if err != nil {
		log.Fatalf("Error calling remote function Debug: %s", err)
	}
	return response
}
