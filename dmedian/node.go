package dmedian

import (
	"fmt"
	"math"
	"math/rand"
)

// MemoryNode implements Node interface by storing data in memory.
type MemoryNode struct {
	data []int
	// name of the node for identification and debugging  purposes
	name string
	// low and high keep offsets for the current state
	low, high int
}

// NewMemoryNode creates new instance memory-based node with initial values properly configured
func NewMemoryNode(name string, data []int) *MemoryNode {
	return &MemoryNode{
		data: data,
		name: name,
		low:  0,
		high: len(data) - 1,
	}
}

// MinMax returns minimum and maximum value for the current state of the node
func (node *MemoryNode) MinMax() (int, int) {
	min := node.data[node.low]
	max := min
	for i := node.low + 1; i <= node.high; i++ {
		if node.data[i] < min {
			min = node.data[i]
		}
		if node.data[i] > max {
			max = node.data[i]
		}
	}
	return min, max
}

// Len returns length of the data in the current state
func (node *MemoryNode) Len() int {
	return node.high - node.low + 1
}

// Pivot returns a value to pivot around
func (node *MemoryNode) Pivot() int {
	return node.data[rand.Intn(node.high-node.low+1)+node.low]
}

func (node *MemoryNode) CountLessThan(target int) int {
	count := 0
	for i := node.low; i <= node.high; i++ {
		if node.data[i] < target {
			count++
		}
	}
	return count
}

func (node *MemoryNode) FilterGreaterOrEq(target int) {
	index := node.partition(target)
	if index == -1 {
		// we have only values greater or equal to target
	} else {
		node.low = index + 1
	}

}

func (node *MemoryNode) FilterLessThan(target int) {
	index := node.partition(target)
	if index == -1 {
		// we have no values left
		node.high = node.low - 1
	} else {
		node.high = index
	}
}

func (node *MemoryNode) Reset() {
	node.low = 0
	node.high = len(node.data) - 1
}

func (node *MemoryNode) Debug() string {
	return fmt.Sprintf("node %s, low: %d, high: %d, data: %+v\n", node.name, node.low, node.high, node.data)
}

// partition moves elements of data so that left side of target contains only lower values and
// right side contains only higher values
func (node *MemoryNode) partition(target int) int {
	pivotPos := node.low
	pivotValue := math.MinInt64
	// find the lowest value that is less than target, a.k.a. the pivot value
	for i := node.low; i <= node.high; i++ {
		if node.data[i] < target && node.data[i] > pivotValue {
			pivotValue = node.data[i]
			pivotPos = i
		}
	}
	if pivotValue == math.MinInt64 {
		//  no values lower than target found
		return -1
	}

	node.swap(node.high, pivotPos)
	partitionIdx := node.low
	// move all values less than or equal to pivot value to the left of pivot value
	for i := node.low; i < node.high; i++ {
		if node.data[i] <= node.data[node.high] {
			node.swap(partitionIdx, i)
			partitionIdx++
		}
	}
	node.swap(node.high, partitionIdx)
	return partitionIdx
}

func (node *MemoryNode) swap(i, j int) {
	node.data[i], node.data[j] = node.data[j], node.data[i]
}
