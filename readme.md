## Requirements
Go 1.11+

## Installing Go
https://golang.org/doc/install

## How to use this program

### Running using `go run`

Use `go run cmd/dmedian/main.go` to run the program directly (with compilation behind the scenes). For example,
assuming we have a file `file.txt`:
```
cat > file.txt <<EOF
1,2,3,4
5,6,7,8
EOF
```
and are working directory is the directory of this file,  we can run the program:

```bash
$ go run cmd/dmedian/main.go file.txt
4.5
```

### Building the program and using the binary

We can also build the program to a static binary and use it instead:

```bash
$ go build -o dm cmd/dmedian/main.go
$ ./dm file.txt
4.5
```

## Implementation

### Algorithm
The code uses modified quick select algorithm to find a median. The select function finds n-th statistic for the data,
median being (n/2)+1 statistic (with odd number of elements) or average of (n/2) and (n/2)+1 statistics (if even number of elements).

Summary of n-th statistic selection algorithm:  
1. Check if minimum and maximum values across the data are equal, or if we are after 1st statistic (which is minimum). If so, minimum is our value.
2. Partition the data using a pivot value (randomly selected element from the input data). Partions are so that the left partition
includes all elements less than pivot value and the right partition includes all elements greater than or equal to pivot value.
3. Count the number of elements less than pivot value. if this number equals sought statistic -1, we found our target. If there are more elements to the left,
it  means the statistic is somewhere there. ignore all elements to the right, go to 1. If there are more elements to the right, ignore all elements to to left, go to 1.

Eventually the Algorithm converges to one of three terminal states:
* the pivot value happens to be the statistic we are looking for
* minimum and maximum values are equal (all elements left in the pool are the same)
* we are looking for 1st statistic, which is minimum.  
 
### Distributed aspect
The code is structured around two main concept: `Node` and `Controller`.
`Node` implements basic operations on data, like finding max/min, partitioning the data using pivot values, etc.
`Controller` implements the entry point, dispatches the work to nodes, and aggregates results returned by them. 

Operations that are distributed to nodes:
* min/max value (controller calculates the min/max returned from nodes)
* counting values less than pivot value
* reducing the search space (each node filters the current state based on the value dispatched by controller)

### Code
* Package `dmedian` includes implementation of `node` and `controller`.
* Package `local` includes median-finding functions based on different implementations. Used for testing and benchmarking.
* Package `remote` includes RPC implementation, allowing the node and the controller to be executed remotely on different machines.
* Package `cmd/dmedian` includes CLI implementation.

## Remote execution
It's possible to execute the node(s) and controller on different machines. Each machine serving as node should
execute following  command:
```bash
./dm -node-only file.txt -port 8080
```

Run above command on different machines, or the same machine using different ports.

Finally, run the controller to calculate the median. If nodes run on the same machine but different ports, use:
```bash
./dm -nodes :8080,:8081,:8082...
```