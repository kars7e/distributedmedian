package remote

import (
	"distributedmedian/dmedian"
	"net"
	"net/http"
	"net/rpc"
)

func NewServer(node dmedian.Node, hostport string) error {
	wrapper := Wrapper{node}
	err := rpc.RegisterName("Node", &wrapper)
	if err != nil {
		return err
	}
	rpc.HandleHTTP()
	l, e := net.Listen("tcp", hostport)
	if e != nil {
		return e
	}
	return http.Serve(l, nil)
}
