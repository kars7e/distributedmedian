package distributedmedian

import (
	"distributedmedian/dmedian"
	"distributedmedian/local"
	"fmt"
	"math"
	"math/rand"
	"testing"
)

// used to ensure compiler does not elide benchmark calls
var tmpResult float64

func BenchmarkNodesNumber(b *testing.B) {
	size := 1000000
	for nodes := 2; nodes < 9; nodes++ {
		ints := generateRandomInts(size, size)
		b.Run(fmt.Sprintf("Distributed_%d", nodes), func(b *testing.B) {
			benchmarkDistributed(b, nodes, ints)
		})
	}
}

func BenchmarkDataSize(b *testing.B) {
	nodes := 4
	for exp := 3; exp < 9; exp++ {
		size := int(math.Pow10(exp))
		ints := generateRandomInts(size, size)
		b.Run(fmt.Sprintf("Sorted_%d", size), func(b *testing.B) {
			benchmarkSorted(b, ints)
		})
		b.Run(fmt.Sprintf("Selection_%d", size), func(b *testing.B) {
			benchmarkSelection(b, ints)
		})
		b.Run(fmt.Sprintf("Distributed_%d", size), func(b *testing.B) {
			benchmarkDistributed(b, nodes, ints)
		})
	}
}

func generateRandomInts(num int, max int) []int {
	list := make([]int, 0, num)
	for i := 0; i < num; i++ {
		list = append(list, rand.Intn(max))
	}
	return list
}

func benchmarkDistributed(b *testing.B, nodesNum int, data []int) {
	c := &dmedian.Controller{}
	for i := 0; i < nodesNum; i++ {
		name := fmt.Sprintf("node%d", i+1)
		nodeData := data[i*(len(data)/nodesNum) : (i+1)*(len(data)/nodesNum)]
		node := dmedian.NewMemoryNode(name, nodeData)
		c.RegisterNode(node)
	}
	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		median := c.Median()
		tmpResult = median
	}
}

func benchmarkSorted(b *testing.B, data []int) {
	for n := 0; n < b.N; n++ {
		datacopy := make([]int, len(data))
		copy(datacopy, data)
		result := local.MedianSorted(datacopy)
		tmpResult = result
	}
}

func benchmarkSelection(b *testing.B, data []int) {
	datacopy := make([]int, len(data))
	copy(datacopy, data)
	for n := 0; n < b.N; n++ {
		result := local.Median(datacopy)
		tmpResult = result
	}

}
