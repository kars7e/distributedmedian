package local

import (
	"math/rand"
	"sort"
)

// Median uses quick select to find the median
func Median(data []int) float64 {
	if len(data)%2 == 0 {
		left := selectNth(data, 0, len(data)-1, len(data)/2)
		right := selectNth(data, 0, len(data)-1, len(data)/2+1)
		if left == right {
			return float64(left)
		} else {
			return float64(left+right) / 2
		}
	}
	return float64(selectNth(data, 0, len(data)-1, len(data)/2+1))
}

func selectNth(data []int, low, high, idx int) int {
	if low == high {
		return data[low]
	}
	pivot := rand.Intn(high-low+1) + low
	partitionIdx := partition(data, low, high, pivot)
	k := partitionIdx - low + 1
	if k == idx {
		return data[partitionIdx]
	} else if k > idx {
		return selectNth(data, low, partitionIdx-1, idx)
	} else {
		return selectNth(data, partitionIdx+1, high, idx-k)
	}
}

func partition(elems []int, low, high, pivot int) int {
	data := sort.IntSlice(elems)
	partitionIdx := low
	data.Swap(pivot, high)
	for i := low; i < high; i++ {
		if elems[i] < elems[high] {
			data.Swap(partitionIdx, i)
			partitionIdx++
		}
	}
	data.Swap(partitionIdx, high)
	return partitionIdx
}

// MedianSorted sorts the slice to find the median
func MedianSorted(data []int) float64 {
	sort.Ints(data)
	if len(data)%2 == 0 {
		left := data[len(data)/2-1]
		right := data[len(data)/2]

		if left == right {
			return float64(left)
		} else {
			return float64(left+right) / 2
		}
	}
	return float64(data[len(data)/2])
}
