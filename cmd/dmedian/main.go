package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"

	"distributedmedian/dmedian"
	"distributedmedian/remote"
)

var nodeOnly = flag.Bool("node-only", false, "Enables node-only mode.")
var port = flag.Int("port", 0, "TCP Port to use when running in node mode.")
var nodeName = flag.String("name", "", "Specify node name when using server node mode.")
var addresses = flag.String("nodes", "", "Specify comma-separated list of nodes (host:port). Enables controller mode.")

func init() {
	flag.Parse()
}

func main() {
	if len(*addresses) != 0 {
		// run in controller mode. Connects to nodes and calculates median using RPC calls.
		controllerMode(strings.Split(*addresses, ","))
		return
	}

	if len(flag.Args()) != 1 {
		log.Fatalf("Missing or extra argument - provide one (and only one) argument, the path to the file containing comma-delimited integers")
		os.Exit(1)
	}
	file, err := os.Open(flag.Args()[0])
	if err != nil {
		log.Fatalf("Error opening file %s: %s", flag.Args()[0], err)
	}
	defer file.Close()

	values := readFile(file)

	var nodes []dmedian.Node
	for i := range values {
		nodes = append(nodes, dmedian.NewMemoryNode(*nodeName, values[i]))
	}
	if len(nodes) == 0 {
		log.Fatalf("No data found in file")
	}

	if *nodeOnly {
		// run in node only mode. start a server that listens for commands coming from controller
		serverMode(nodes[0])
		return
	}

	// run in local mode, all nodes are created in memory.
	localMode(nodes)

}

func serverMode(node dmedian.Node) {
	log.Printf("Running in node server mode...")
	err := remote.NewServer(node, ":"+strconv.Itoa(*port))
	if err != nil {
		log.Fatalf("Error returned by RPC server: %s", err)
	}
}

func controllerMode(nodes []string) {
	if len(nodes) == 0 {
		log.Fatalf("no nodes provided")
	}
	log.Printf("Running in controller mode...")
	controller := dmedian.Controller{}
	for _, n := range nodes {
		node, err := remote.NewNode(n)
		if err != nil {
			log.Fatalf("Error connecting with node %s: %s", n, err)
		}
		controller.RegisterNode(node)
	}
	fmt.Println(controller.Median())
}

func localMode(nodes []dmedian.Node) {
	if len(nodes) == 0 {
		log.Fatalf("no nodes provided")
	}
	controller := dmedian.Controller{}
	for _, node := range nodes {
		controller.RegisterNode(node)
	}
	fmt.Println(controller.Median())
}

func readFile(file io.Reader) [][]int {
	var result [][]int

	reader := bufio.NewReader(file)

	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			break
		}
		line = line[:len(line)-1]
		if len(line) != 0 {
			values := strToInt(strings.Split(line, ","))
			result = append(result, values)
		}
	}
	return result
}

func strToInt(values []string) []int {
	ints := make([]int, 0, len(values))
	for _, str := range values {
		val, err := strconv.Atoi(str)
		if err != nil {
			log.Fatalf("error converting value %s: %s", str, err)
		}
		ints = append(ints, val)
	}
	return ints
}
